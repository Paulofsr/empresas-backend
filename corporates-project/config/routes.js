/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

 const settings = require('../settings');
 const prefix = '/api/v';
 var routes = {};
 routes[`POST ${prefix + settings.apiVersion}/users/auth/sign_in`] = 'UsersController.login';
 routes[`POST ${prefix + settings.apiVersion}/users/register`] = 'UsersController.create';

module.exports.routes = routes;

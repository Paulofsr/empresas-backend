/**
 * HTTP Server Settings
 * (sails.config.http)
 *
 * Configuration for the underlying HTTP server in Sails.
 * (for additional recommended settings, see `config/env/production.js`)
 *
 * For more information on configuration, check out:
 * https://sailsjs.com/config/http
 */
var fs = require('fs');
var path = require('path');

module.exports.ssl = {
  key: fs.readFileSync(path.resolve(__dirname,'./ssl/private.key')),
  cert: fs.readFileSync(path.resolve(__dirname,'./ssl/certificate.pem'))
};

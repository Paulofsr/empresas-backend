const passport = require('passport'),
      LocalStrategy = require('passport-local').Strategy,
      bcrypt = require('bcrypt-nodejs');
const passportJWT = require("passport-jwt");
const JWTStrategy   = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
const settings = require('../settings');

passport.serializeUser(
    (user, cb) => {
        cb(null, user.id);
    }
);

passport.deserializeUser(
    (id, cb) => {
        Users.findOne({id}, 
            (err, users) => {
                cb(err, users);
            }
        );
    }
);

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    }, 
    function(email, password, done) {
        Users.findOne({email: email}, function(err, user){
            if(err) return done(err);
            if(!user) return done(null, false, {message: 'Username not found'});
            bcrypt.compare(password, user.password, function(err, res){
                if(!res) return done(null, false, { message: 'Invalid Password' });
                let userDetails = {
                        email: user.email,
                        name: user.name,
                        id: user.id
                    };
                return done(null, userDetails, { message: 'Login Succesful'});
            });
        });
    })
);
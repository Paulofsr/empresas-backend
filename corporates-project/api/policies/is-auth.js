var jwt = require('jsonwebtoken');
const settings = require('../../settings');

module.exports = (req, res, next) => {
    if (req.headers && req.headers['access-token'] && req.headers['client'] && req.headers['uid']) {
        let parts = req.headers['access-token'].split(' ');
        if (parts.length == 2) {
            let scheme = parts[0];
            let credentials = parts[1];
            if (/^Bearer$/i.test(scheme)) {
                jwt.verify(credentials, settings.secretKey, function(err, user) {
                    if (err) {
                        return res.status(401).json(err);
                    }
                    req.user = user;
                    res.setHeader('Content-Type', 'application/json');
                    res.setHeader('access-token', credentials);
                    res.setHeader('client', user.name);
                    res.setHeader('uid', user.id);
                    next();
                });
            } else {
                return res.status(401).json({ 
                    err: 'Bearer not found"'
                });
            }
        } else {
            return res.status(401)
            .json({
                err: 'Invalid format "access-token: Bearer [token]"'
            })
            ;
        }
    } else {
        return res.status(401)
        .json({
            err: 'Paramters not found in the Header: "access-token", "client" and "uid"'
        })
        ;
    }
}
/**
 * UsersController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const passport = require('passport');
const jwt = require('jsonwebtoken');
const settings = require('../../settings');

module.exports = {
  
    login: (req, res) => {
        passport.authenticate('local', (err, user, info) => {
        if((err) || (!user)) {
            return res.send({
            message: info.message,
            user
            });            
        }
        const token = jwt.sign(user, settings.secretKey, {expiresIn: 3600});
        res.setHeader('Content-Type', 'application/json');
        res.setHeader('access-token', token);
        res.setHeader('client', user.name);
        res.setHeader('uid', user.id);
        req.logIn(user, (err) => {
                if(err) res.send(err);
                return res.send({
                    message: info.message,
                    user,
                    token
                });
            });
            })(req, res);
    }

};


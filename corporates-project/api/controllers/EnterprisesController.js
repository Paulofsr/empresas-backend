/**
 * EnterprisesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    find: (req, res) => {
        var filter = {};
        if(req.query){
            if(req.query.enterprise_types) filter['type'] = req.query.enterprise_types;
            if(req.query.name) filter['name'] = { 'contains': req.query.name };
        }
        Enterprises
            .find(filter)
            .populate('type')
            .exec((err, enterprises) => {
                if(err) res.send(err);
                else res.send(enterprises);
            });
    }
};


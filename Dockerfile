FROM alpine

RUN apk update && \
    apk add nodejs && \
    apk add nodejs-npm && \
    mkdir /app && \
    npm install sails -g

WORKDIR /app

ENV DB_USER='1Oi0IwsPPX' \
    DB_PSWD='qHkOOmjkd7' \
    DB_HOST='remotemysql.com' \
    DB_PORT='3306' \
    DB_NAME='1Oi0IwsPPX' 

COPY ./corporates-project .

RUN npm i 

EXPOSE 80

CMD [ "npm", "start" ]